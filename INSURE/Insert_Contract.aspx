﻿<%@ Page Title="Insert New Contract Details" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Insert_Contract.aspx.cs" Inherits="INSURE.Insert_Contract" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        New Contract
    </h2>
    <h3>
        Customer Info
    </h3>
    <div><p>Name : </p><input id="customer_name_input" type="text" /></div>
    <div><p>Surname : </p><input id="customer_surname_input" type="text" /></div>
    <div><p>Address : </p><input id="customer_address_input" type="text" /></div>
    <div><p>Telephone : </p><input id="customer_telephone_input" type="text" /></div>
    <div><p>E-mail : </p><input id="customer_email_input" type="text" /></div>
    <div><p>Medical record : </p><input id="contract_medical_record_input" type="text" /></div>
    
    <h3>
        Contract Info
    </h3>
    <div><p>Treatment position : </p>
    <select id="contract_treatment_position_input">
        <option>A</option>
        <option>B</option>
        <option>C</option>
    </select></div>
    <div><p>Insurance premium : </p><input id="contract_insurance_premium_input" type="text" /></div>
    <div><p>Insurance payment date : </p><input id="contract_insurance_payment_date_input" type="text" /></div>
    <div><p>Insurance payment method : </p>
    <select id="contract_insurance_payment_method_input">
        <option>Cash</option>
        <option>Credit Card</option>
        <option>Paypal</option>
    </select></div>
</asp:Content>