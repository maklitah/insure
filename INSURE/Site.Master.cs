﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace INSURE
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.User.Identity.IsAuthenticated) {
                NavigationMenu.Visible = true;
            } else {
                List<MenuItem> nonViewItems = new List<MenuItem>();

                foreach (MenuItem currentItem in NavigationMenu.Items) {
                    if (currentItem.Text != "About") {
                        nonViewItems.Add(currentItem);
                    }
                }

                for (int itemIndex = 0; itemIndex < nonViewItems.Count; itemIndex++) {
                    NavigationMenu.Items.Remove(nonViewItems.ElementAt(itemIndex));
                }
            }
        }
    }
}
